var parseurl = require('parseurl');
var md5 = require('md5');
var SECRET_KEY="MfdFC9VbbKegBCaYs5kSXJZBqJhVHwNBpAWYLm3t8PLmCtavng";
var crypto = require('crypto'),
    algorithm = 'aes192';
var fs = require('fs');
var cors = require('cors');
var session = require('express-session');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var http = require('http');
var https = require('https');

const jwt  = require('jsonwebtoken');
var fileUpload = require('express-fileupload');

const express = require('express'),
  app = express(),
  bodyParser = require('body-parser');
  port = process.env.PORT || 8600;
  portssl=8666;
  app.use(cors());
  app.use(fileUpload());
  app.use(session({
		secret: 'MfdFC9VbbKegBCaYs5kSXJZBqJhVHwNBpAWYLm3t8PLmCtadgh',
		resave: false,
		saveUninitialized: true
	}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next();
	});

//uncomment options for ssl
//var options = {
//    key: fs.readFileSync('/domain.com.key'),
//    cert: fs.readFileSync('/certificate.crt')
//};
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Credentials', false);
    res.header('Access-Control-Max-Age', '8600');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, x_chord, y_chord, z_chord, d_chord');
    next();
};

var encrypt = function(text){
    var cipher = crypto.createCipher(algorithm,SECRET_KEY);
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
};

var decrypt = function(text){
    var decipher = crypto.createDecipher(algorithm,SECRET_KEY);
    var dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
};


const jwtKey = 'MfdFC9VDDKegBCaYs5kSXJZBqJhVHwNBpAWYLm3t8PLmCtadgh'
const jwtExpirySeconds = 200000 // 55.56 hours


const users = {
  postcode_admin: {password:'postcode_admin', id:'sample_faf34244354', role:'admin'},
  postcode_user: {password:'postcode_pass', id:'sample_faf34244355', role:'member'}
}


app.post('/token', function(req, res){
    const { username, password } = req.body
    // console.log("inside the api")
    console.log( users[username])
    if (!username || !password || users[username].password !== password) {
      // return 401 error is username or password doesn't exist, or if password does
      // not match the password in our records
      return res.status(401).end()
    }
   let { id,role } = users[username];
    // Create a new token with the username in the payload
    const token = jwt.sign({ username,id, role }, jwtKey, {
      algorithm: 'HS256',
      expiresIn: jwtExpirySeconds
    })
    // let accessToken = 'Bearer ' + token
    console.log('token:', token)
    res.status(200).send({"Status":"Success","Code":1,"Message":"Token Generated","Document":token});
   
});


app.get('/', (req, res) => {
  res.send('UK Postcode API!')
})

// Favicon
app.use('/favicon.ico', express.static('favicon.ico'));

// Development Logger
// app.use(logger('dev'));

// Production Logger
app.use(logger('prod'));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(allowCrossDomain);
  require('./oracle-db').then(res=>{
    
    http.createServer(app).listen(port);
    console.log('API server started on: ' + port);
   
    // console.log(l.Update(["'demo_data7'"],'PAF_BUILDING_NAME','BNA_ID',7))
  }).catch(err =>{
    console.log(err)
  })


// Create an HTTPS service identical to the HTTP service.
//https.createServer(options, app).listen(portssl);
//app.listen(port);



var apiRoutes = require('./app/routes/apiRoute');
// const { log } = require('console');
// const Api = require('./app/models/API');
// const connection = require('./oracle-db');

apiRoutes(app)

process.on('SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here
  process.exit(1);
});

