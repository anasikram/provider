'user strict';
const Api = require('./API')
var url = require('url');
var helper = require('./helper');
const cons = require('consolidate');
const { autoCommit } = require('oracledb');
const { PAF_LOCALITY, PAF_BUILDING_NAME, PAF_SUB_BUILDING_NAME, PAF_THOROUGHFARE, PAF_THOROUGHFARE_DESCRIPTOR, PAF_MAILSORT, PAF_ORGANISATION, PAF_EDITION } = require('./classes');
// var oracle = connection();

class PAF_ADDRESS {
    
    constructor(api) {
    
        
        this.building_name = new PAF_BUILDING_NAME(api,api.FK_BNA_ID);
        this.sub_building_name = new PAF_SUB_BUILDING_NAME(api,api.FK_SBN_ID)
        this.thoroughfare = new PAF_THOROUGHFARE(api,api.FK_THO_ID)
        this.thoroghfare_desc = new PAF_THOROUGHFARE_DESCRIPTOR(api,api.FK_THD_ID)
        this.mail_sort = new PAF_MAILSORT(api,null)
        this.organisation = new PAF_ORGANISATION(api,api.FK_ORG_ID)
        this.locality = new PAF_LOCALITY(api,api.FK_LOC_ID)
        this.edition = new PAF_EDITION(api,null);
        //PAF_ADDRESS
        this.table = 'PAF_ADDRESS'
        this.Column = 'ADDRESS_ID'
        this.id = api.ADDRESS_ID
        this.postcode = api.postcode;
        this.outcome = api.outcome;
        this.incode = api.incode;
        this.address_id = api.ADDRESS_ID||null
        this.FK_LOC_ID = api.FK_LOC_ID||null
        this.FK_THO_ID = api.FK_THO_ID||null
        this.FK_THD_ID = api.FK_THD_ID||null
        this.dependent_thoroughfare = api.dependent_thoroughfare;
        this.dependent_thoroughfare_descriptor = api.dependent_thoroughfare_descriptor;
        this.building_number = api.building_number;
        this.FK_BNA_ID = api.FK_BNA_ID||null
        this.FK_SBN_ID = api.FK_SBN_ID||null
        this.number_of_households = api.number_of_households;
        this.FK_ORG_ID = api.FK_ORG_ID||null;
        this.FK_ORG_POSTCODE_TYPE = api.postcode_type;
        this.concatenation_index = api.concatenation_index
        this.delivery_point_suffix = api.delivery_point_suffix;
        this.smalluser_organisation_indicator = api.smalluser_organisation_indicator;
        this.po_box_number = api.po_box_number;
        this.welsh = api.welsh
        this.argument = []
        
    }
    async InsertRecord(){
        try{
            let fk_loc = await this.locality.Insert();
            let fk_org = await this.organisation.Insert();
            let fk_tho = await this.thoroughfare.Insert();
            let fk_thd = await this.thoroghfare_desc.Insert();
            let fk_bn = await this.building_name.Insert();
            let fk_sbn = await this.sub_building_name.Insert();
            this.address_id = await Api.generateIndex();
            let mail_sort = await this.mail_sort.Insert();
    //         // let edition = await this.edition.Insert();
            
            let argument = [
                `'${this.postcode}'`,`'${this.outcome}'`,`'${this.incode}'`,`${this.address_id}`,`${fk_loc}`,`${fk_tho}`,`${fk_thd}`,`${this.dependent_thoroughfare}`,
        `${this.dependent_thoroughfare_descriptor}`,`${this.building_number}`,`${fk_bn}`,`${fk_sbn}`,`${this.number_of_households}`,
        `${fk_org}`,`'${this.FK_ORG_POSTCODE_TYPE}'`,`'${this.concatenation_index}'`,`'${this.delivery_point_suffix}'`,
        `'${this.smalluser_organisation_indicator}'`,`'${this.po_box_number}'`,`'${this.welsh}'`
            ]
            this.argument = argument;
      let result = await Api.Insert2(this);
      if(result){
          return result
      }else {
          await Api.Delete(this.locality.table,this.locality.Column,this.locality.id)
          await Api.Delete(this.organisation.table,this.organisation.Column,this.organisation.id)
          await Api.Delete(this.thoroughfare.table,this.thoroughfare.Column,this.thoroughfare.id)
          await Api.Delete(this.thoroghfare_desc.table,this.thoroghfare_desc.Column,this.thoroghfare_desc.id)
          await Api.Delete(this.building_name.table,this.building_name.Column,this.building_name.id)
          await Api.Delete(this.sub_building_name.table,this.sub_building_name.Column,this.sub_building_name.id)
        //   await Api.Delete(this.mail_sort.table,this.mail_sort.Column,this.mail_sort.id)
        //   await Api.Delete(this.edition.table,this.edition.Column,this.edition.id)
        return null

      }

        }
        catch(err){
            console.log(err)
            await Api.Delete(this.locality.table,this.locality.Column,this.locality.id)
            await Api.Delete(this.organisation.table,this.organisation.Column,this.organisation.id)
            await Api.Delete(this.thoroughfare.table,this.thoroughfare.Column,this.thoroughfare.id)
            await Api.Delete(this.thoroghfare_desc.table,this.thoroghfare_desc.Column,this.thoroghfare_desc.id)
            await Api.Delete(this.building_name.table,this.building_name.Column,this.building_name.id)
            await Api.Delete(this.sub_building_name.table,this.sub_building_name.Column,this.sub_building_name.id)
            throw err
        }
    }
    async UpdateRecord(){
        try{
            let fk_loc = await this.locality.Update();
            let fk_org = await this.organisation.Update();
            let fk_tho = await this.thoroughfare.Update();
            let fk_thd = await this.thoroghfare_desc.Update();
            let fk_bn = await this.building_name.Update();
            let fk_sbn = await this.sub_building_name.Update();
            let mail_sort = await this.mail_sort.Update();
            // let edition = await this.edition.Insert();
            
            let argument = [
                `'${this.postcode}'`,`'${this.outcome}'`,`'${this.incode}'`,`${this.address_id}`,`${this.FK_LOC_ID}`,`${this.FK_THO_ID }`,`${this.FK_THD_ID}`,`${this.dependent_thoroughfare}`,
        `${this.dependent_thoroughfare_descriptor}`,`${this.building_number}`,`${this.FK_BNA_ID}`,`${this.FK_SBN_ID}`,`${this.number_of_households}`,
        `${this.FK_ORG_ID}`,`'${this.FK_ORG_POSTCODE_TYPE}'`,`'${this.concatenation_index}'`,`'${this.delivery_point_suffix}'`,
        `'${this.smalluser_organisation_indicator}'`,`'${this.po_box_number}'`,`'${this.welsh}'`
            ]
            this.argument = argument;
      let result = await Api.Update2(this);
      if(result){
          return result
      }else {
          
        return null

      }

        }
        catch(err){
            console.log(err)
            throw err
        }
    }
    static async DeleteRecord(id){
     try{   let obj = new PAF_ADDRESS((await Api.getById(id,true))[0])
// console.log(obj)
         await Api.Delete(obj.locality.table,obj.locality.Column,obj.locality.id)
          await Api.Delete(obj.organisation.table,obj.organisation.Column,obj.organisation.id)
          await Api.Delete(obj.thoroughfare.table,obj.thoroughfare.Column,obj.thoroughfare.id)
          await Api.Delete(obj.thoroghfare_desc.table,obj.thoroghfare_desc.Column,obj.thoroghfare_desc.id)
          await Api.Delete(obj.building_name.table,obj.building_name.Column,obj.building_name.id)
          await Api.Delete(obj.sub_building_name.table,obj.sub_building_name.Column,obj.sub_building_name.id)
          await Api.Delete(obj.table,obj.Column,obj.address_id)
        }

    
    catch(err){
        throw err
    }
    

}
}


// let postcode ={

//         building_name : "api.building_name",
//         sub_building_name : "api",
      
//         thoroughfare_name : "api.thoroughfare_name",
        
//         thoroughfare_descriptor_name : "api.thoroughfare",
//         approved_abbereviation : "api",
        
//         outcode : "api.outcode",
//         postcode_sector : "api.postcode_sector",
//         residue_identifier : "api.residue_identifier",
//         direct_within_residue_ident : "api.direct_within_residue_ident",
     
//         postcode_type : "S",
//         organisation_name : "Fiverr.com",
//         department_name : "",
        
//         post_town : "api.post_town",
//         dependent_locality : "api.dependent_locality",
//         double_dependent_locality : "pi.double_dependent_locality",
    
//         postcode : "AB12",
//         outcome : "AF12",
//         incode : "a",
//         address_id : 44123,
//         FK_LOC_ID : null,
//         FK_THO_ID : null,
//         FK_THD_ID : null,
//         dependent_thoroughfare : 12,
//         dependent_thoroughfare_descriptor : 3,
//         building_number : 23,
//         FK_BNA_ID : null,
//         FK_SBN_ID : null,
//         number_of_households : 4,
//         FK_ORG_ID : null,
//         FK_ORG_POSTCODE_TYPE : "S",
//         concatenation_index : "i",
//         delivery_point_suffix : "ab",
//         smalluser_organisation_indicator : "i",
//         po_box_number : "na",
//         welsh : "F"
// }



module.exports= PAF_ADDRESS;
