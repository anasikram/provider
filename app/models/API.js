var connection = require('../../oracle-db');
class Api{
    static async Insert(obj){
        console.log(obj.table)
        let { argument,table,Column } = obj
        connection = (await connection);
      
         try{
             let query1 = `SELECT MAX(${Column})AS id FROM ${table}`;
           
         let result  = await connection.execute(query1);
         let id = parseInt(((Math.random() * 20)+1))+result.rows[0].ID||0
        //   console.log(result)
         let values = `${id},`
         
         argument.forEach((element,index) => {
            //  console.log(element)
             if(index<(argument.length-1))
             values += element + ','
             else
             values += element
         });
        //  console.log("values")
        
         let query2 = `INSERT INTO ${table} VALUES(${values})`
        
         result = await connection.execute(query2);
         console.log(result)
         
         if(result.rowsAffected)
             return id
         else 
         return null
         
 
         }
         catch(err){
             throw err
         }
     }
     static async Update(obj){
         let { argument,table,Column,id } = obj
         connection = (await connection);
         try{
             await Api.Delete(table,Column,id)
         
          console.log(id)
         let values = `${id},`
         argument.forEach((element,index) => {
             if(index<(argument.length-1))
             values += element + ','
             else
             values += element
         });
         let query2 = `INSERT INTO ${table} VALUES(${values})`
         console.log(query2)
         let result = await connection.execute(query2);
         
         console.log(result)
         if(result.rowsAffected)
             return id
         else 
         return null
         
 
         }
         catch(err){
             throw err
         }
     }
     static async Delete(table,Column,id){
         connection = (await connection);
         try{
             let query1 = `DELETE FROM ${table} WHERE ${Column}=${id} `; 
             let result  = await connection.execute(query1);
             console.log(result)
             return id
         
 
         }
         catch(err){
             throw err
         }
     }
     static async getById(id,single){
         connection = (await connection);
         try{
             let query1 = `
             SELECT * FROM PAF_ADDRESS pa,PAF_BUILDING_NAME pbn ,PAF_LOCALITY pl ,PAF_ORGANISATION po ,
             PAF_SUB_BUILDING_NAME psbn,PAF_THOROUGHFARE pt ,PAF_THOROUGHFARE_DESCRIPTOR ptd  WHERE pa.ADDRESS_ID = ${parseInt(id)} 
             AND pa.FK_BNA_ID = pbn.BNA_ID AND pa.FK_LOC_ID = pl.LOC_ID AND pa.FK_THO_ID = pt.THO_ID 
             AND pa.FK_THD_ID = ptd.THD_ID AND pa.FK_SBN_ID = psbn.SBN_ID AND pa.FK_ORG_ID = po.ORG_ID  
             `; 
             if(single)
             query1 +=' FETCH FIRST 1 ROWS ONLY'
             let result  = await connection.execute(query1);
             return result.rows
         
 
         }
         catch(err){
             throw err
         }
     }
     static async getByPostal(postal,single){
         connection = (await connection);
         try{
             let query1 = `
             SELECT * FROM PAF_ADDRESS pa,PAF_BUILDING_NAME pbn ,PAF_LOCALITY pl  ,PAF_ORGANISATION po ,
             PAF_SUB_BUILDING_NAME psbn,PAF_THOROUGHFARE pt ,PAF_THOROUGHFARE_DESCRIPTOR ptd  WHERE pa.POSTCODE = '${postal}' 
             AND pa.FK_BNA_ID = pbn.BNA_ID AND pa.FK_LOC_ID = pl.LOC_ID AND pa.FK_THO_ID = pt.THO_ID 
             AND pa.FK_THD_ID = ptd.THD_ID AND pa.FK_SBN_ID = psbn.SBN_ID AND pa.FK_ORG_ID = po.ORG_ID 
             `; if(single==='true')
             query1 +=' FETCH FIRST 1 ROWS ONLY'
             let result  = await connection.execute(query1);
             console.log(result.rows)
             return result.rows
         
 
         }
         catch(err){
             throw err
         }
     }
     static async getAll(){
         connection = (await connection);
         try{
             let query1 = `
             SELECT * FROM PAF_ADDRESS pa,PAF_BUILDING_NAME pbn ,PAF_LOCALITY pl ,PAF_ORGANISATION po ,
             PAF_SUB_BUILDING_NAME psbn,PAF_THOROUGHFARE pt ,PAF_THOROUGHFARE_DESCRIPTOR ptd  WHERE 
             pa.FK_BNA_ID = pbn.BNA_ID AND pa.FK_LOC_ID = pl.LOC_ID AND pa.FK_THO_ID = pt.THO_ID 
             AND pa.FK_THD_ID = ptd.THD_ID AND pa.FK_SBN_ID = psbn.SBN_ID AND pa.FK_ORG_ID = po.ORG_ID 
             `; 
             let result  = await connection.execute(query1);
             console.log(result.rows)
             return result.rows
         
 
         }
         catch(err){
             throw err
         }
     }
     static async Insert2(obj){
         console.log(obj.argument)
         let { argument,table} = obj
         connection = (await connection);
          try{
           
          let values = ''
          argument.forEach((element,index) => {
              if(index<(argument.length-1))
              values += element + ','
              else
              values += element
          });
          let query2 = `INSERT INTO ${table} VALUES(${values})`
         
         let result = await connection.execute(query2);
          
          console.log(result)
          if(result.rowsAffected)
              return result.rowsAffected
          else 
          return null
          
  
          }
          catch(err){
              throw err
          }
      }
      static async Update2(obj){
          let { argument,table,Column,id } = obj
          connection = (await connection);
          try{
              await Api.Delete(table,Column,id)
          
           
          let values = ''
          argument.forEach((element,index) => {
              if(index<(argument.length-1))
              values += element + ','
              else
              values += element
          });
          let query2 = `INSERT INTO ${table} VALUES(${values})`
         
          let result = await connection.execute(query2);
          
          
          if(result.rowsAffected)
              return id
          else 
          return null
          
  
          }
          catch(err){
              throw err
          }
      }
      static async generateIndex(){
        try{let query1 = `SELECT MAX(ADDRESS_ID)AS id FROM PAF_ADDRESS`;
           
        let result  = await connection.execute(query1);
        let id = result.rows[0].ID
        return parseInt(((Math.random() * 50)+1))+id||0
    }
        catch(err){
            return Math.random() * 40000
        }
      }
}

module.exports = Api