const Joi = require('joi');
exports.record = Joi.object()
  .keys({
    

    building_name:Joi.string().required().max(50) ,
    sub_building_name:Joi.string().required().max(30) ,
  
    thoroughfare_name:Joi.string().required().max(60) ,
    
    thoroughfare_descriptor_name:Joi.string().required().max(20) ,
    approved_abbereviation:Joi.string().required().max(6) ,
    
    outcode:Joi.string().required().max(4) ,
    postcode_sector:Joi.string().required().max(1) ,
    residue_identifier:Joi.string().required().max(3) ,
    direct_within_residue_ident:Joi.string().required().max(2) ,
 
    postcode_type:Joi.string().max(1) ,
    organisation_name:Joi.string().required().max(60) ,
    department_name:Joi.string().required().max(60) ,
    
    post_town:Joi.string().required().max(30) ,
    dependent_locality:Joi.string().max(35) ,
    double_dependent_locality:Joi.string().max(35) ,

    postcode:Joi.string().required().max(7) ,
    outcome:Joi.string().required().max(4) ,
    incode:Joi.string().required().max(3) ,
    
    building_number:Joi.number() ,
   
    number_of_households:Joi.number() ,
    FK_ORG_POSTCODE_TYPE:Joi.string().max(1) ,
    concatenation_index:Joi.string().required().max(1) ,
    delivery_point_suffix:Joi.string().required().max(2) ,
    smalluser_organisation_indicator:Joi.string().max(1) ,
    po_box_number:Joi.string().required().max(6) ,
    welsh:Joi.string().required().valid('F','T').max(1)
  })
  .unknown(true);

