const Api = require('./API')

class PAF_BUILDING_NAME{
    constructor(api,id){
        this.table = 'PAF_BUILDING_NAME';
        this.Column = 'BNA_ID'
        this.building_name = api.building_name;
        this.argument = [`'${api.building_name}'`]
        this.id = id||null
    }
    async Insert(){
      try{
          let id = Api.Insert(this);
          this.id = id;
          return id;
    }
    catch(err){
        throw err
    }
    }
    async Update(){

      try{
        Api.Update(this);
        
  }
  catch(err){
      throw err
  }
    }
}
class PAF_SUB_BUILDING_NAME{
    constructor(api,id){
        this.table = 'PAF_SUB_BUILDING_NAME';
        this.Column = 'SBN_ID'
        this.sub_building_name = api.sub_building_name;
        this.argument = [`'${api.sub_building_name}'`]
        this.id = id||null
    }
    async Insert(){
      try{
          let id = Api.Insert(this);
          this.id = id;
          return id;
    }
    catch(err){
        throw err
    }
    }
    async Update(){

      try{
        Api.Update(this);
        
  }
  catch(err){
      throw err
  }
    }
}
class PAF_THOROUGHFARE{
    constructor(api,id){
        this.table = 'PAF_THOROUGHFARE';
        this.Column = 'THO_ID'
        this.thoroughfare_name = api.thoroughfare_name
        this.argument = [`'${api.thoroughfare_name}'`]
        this.id = id||null
    }
    async Insert(){
      try{
          let id = Api.Insert(this);
          this.id = id;
          return id;
    }
    catch(err){
        throw err
    }
    }
    async Update(){

      try{
        Api.Update(this);
        
  }
  catch(err){
      throw err
  }
    }
}
class PAF_THOROUGHFARE_DESCRIPTOR{
    constructor(api,id){
        this.table = 'PAF_THOROUGHFARE_DESCRIPTOR';
        this.Column = 'THD_ID'
        this.thoroughfare_descriptor_name = api.thoroughfare_descriptor_name;
        this.approved_abbereviation = api.approved_abbereviation;
        this.argument = [`'${api.thoroughfare_descriptor_name}'`,`'${api.approved_abbereviation}'`]
        this.id = id||null
    }
    async Insert(){
      try{
          let id = Api.Insert(this);
          this.id = id;
          return id;
    }
    catch(err){
        throw err
    }
    }
    async Update(){

      try{
        Api.Update(this);
        
  }
  catch(err){
      throw err
  }
    }
}
class PAF_MAILSORT{
    constructor(api,id){
        this.table = 'PAF_MAILSORT';
        this.Column = 'OUTCODE'
        this.outcode = api.outcode;
        this.postcode_sector = api.postcode_sector
        this.residue_identifier = api.residue_identifier;
        this.direct_within_residue_ident = api.direct_within_residue_ident;
        this.argument = [`'${api.outcode}'`,`'${api.postcode_sector}'`,`'${api.residue_identifier}'`,`'${api.direct_within_residue_ident}'`]
        this.id = id||`'${api.outcode}'`
    }
    async Insert(){
      try{
          let id = Api.Insert2(this);
          
    }
    catch(err){
        throw err
    }
    }
    async Update(){

      try{
        Api.Update2(this);
        
  }
  catch(err){
      throw err
  }
    }
}
class PAF_ORGANISATION{
    constructor(api,id){
        this.table = 'PAF_ORGANISATION';
        this.Column = 'ORG_ID'
        this.postcode_type = api.postcode_type;
        this.organisation_name = api.organisation_name;
        this.department_name = api.department_name;
        this.argument = [`'${api.postcode_type}'`,`'${api.organisation_name}'`,`'${api.department_name}'`]
        this.id = id||null
    }
    async Insert(){
      try{
          let id = Api.Insert(this);
          this.id = id;
          return id;
    }
    catch(err){
        throw err
    }
    }
    async Update(){

      try{
        Api.Update(this);
        
  }
  catch(err){
      throw err
  }
    }
}
class PAF_LOCALITY{
    constructor(api,id){
        this.table = 'PAF_LOCALITY';
        this.Column = 'LOC_ID'
        this.post_town = api.post_town;
        this.dependent_locality = api.dependent_locality;
        this.double_dependent_locality = api.double_dependent_locality;
        this.argument = [`'${api.post_town}'`,`'${api.dependent_locality}'`,`'${api.double_dependent_locality}'`]
        this.id = id||null
    }
    async Insert(){
      try{
          let id = Api.Insert(this);
          this.id = id;
          return id;
    }
    catch(err){
        throw err
    }
    }
    async Update(){

      try{
        Api.Update(this);
        
  }
  catch(err){
      throw err
  }
    }
}
class PAF_EDITION{
    constructor(api,id){
        this.table = 'PAF_EDITION';
        this.Column = ''
        this.file_loaded = api.file_loaded;
        this.edition = api.edition
        this.date_loaded = api.date_loaded
        this.argument = [`'${api.file_loaded}'`,`'${api.edition}'`,`'${api.date_loaded}'`]
        this.id = id||null
    }
    async Insert(){
      try{
          let id = Api.Insert2(this);
          this.id = id;
          return id;
    }
    catch(err){
        throw err
    }
    }
    async Update(){

      try{
        Api.Update2(this);
        
  }
  catch(err){
      throw err
  }
    }
}
module.exports = {    
    PAF_BUILDING_NAME,PAF_SUB_BUILDING_NAME,PAF_THOROUGHFARE,PAF_THOROUGHFARE_DESCRIPTOR,PAF_MAILSORT,
    PAF_ORGANISATION,PAF_LOCALITY,PAF_EDITION

}