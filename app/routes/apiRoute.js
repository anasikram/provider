'use strict';
module.exports = (app) => {
  const apiInstance = require('../controllers/apiController');
  const { isAuth, isAdmin,ValidateApiSchema } = require('../models/middlewares');
  const { record } = require('../models/apiSchema')

  app.route('/api/search')
    .get(isAuth, apiInstance.readByPostcode);
  // todoList Routes
  app.route('/api')
    .get(apiInstance.listAll)
    .post(isAuth, isAdmin,ValidateApiSchema(record),apiInstance.createNew);

  app.route('/api/:id')
    .get(isAuth, apiInstance.readById)
    .put(isAuth, isAdmin,ValidateApiSchema(record), apiInstance.updateById)
    .delete(isAuth, isAdmin, apiInstance.deleteById);

};

    