'use strict';
var helper = require('../models/helper.js');
const PAF_ADDRESS = require('../models/apiModel.js');
const Api = require('../models/API.js');


exports.listAll = function(req, res) {
  Api.getAll().then(response => {
    res.status(200).send(helper.createResponse(helper.Success,1,"Record found",response));
  }).catch(err => {
    res.status(200).send(helper.createResponse(helper.Error,0,err,"Something went wrong"));
  })
};

exports.createNew = function(req, res) {

     
  var reqObj = new PAF_ADDRESS(req.body);

  reqObj.InsertRecord().then(response =>{
    if(res)
    res.status(200).send(helper.createResponse(helper.Success,1,"Record Created",reqObj));
    else
    res.status(200).send(helper.createResponse(helper.Error,0,"Something went wrong"));
  }).catch(err=>{
    res.status(200).send(helper.createResponse(helper.Error,0,err,""));
  })


   
};


exports.readById = function(req, res) {

  Api.getById(req.params.id,true).then(response => {
    res.status(200).send(helper.createResponse(helper.Success,1,"Record found",response[0]));
  }).catch(err => {
    res.status(200).send(helper.createResponse(helper.Error,0,err,"Something went wrong"));
  })

};

exports.readByPostcode = function(req, res) {

    console.log(req.query)
    let  { postcode,single } = req.query;
    
    if(req.query.postcode)
    Api.getByPostal(postcode,single||false).then(response => {
      if(single==='true')
      res.status(200).send(helper.createResponse(helper.Success,1,"Record found",response[0]));
      else
      res.status(200).send(helper.createResponse(helper.Success,1,"Record found",response));
    }).catch(err => {
      res.status(200).send(helper.createResponse(helper.Error,0,err,"Something went wrong"));
    })
    

  };

exports.updateById = function(req, res) {
       let obj = req.body;
  Api.getById(req.params.id,true).then(response => {
    obj = {
      ...obj,...response[0]
    }
    var reqObj = new PAF_ADDRESS(obj);
    reqObj.UpdateRecord().then(response =>{
      if(response)
      res.status(200).send(helper.createResponse(helper.Success,1,"Record Updated",reqObj));
      else
      res.status(200).send(helper.createResponse(helper.Error,0,"Something went wrong"));
    }).catch(err=>{
      res.status(200).send(helper.createResponse(helper.Error,0,err,""));
    })

  }).catch(err => {
    res.status(200).send(helper.createResponse(helper.Error,0,err,"Record not found"));
  })

    
};


exports.deleteById = function(req, res) {
  
  PAF_ADDRESS.DeleteRecord(parseInt(req.params.id)).then(response=>{
    res.status(200).send(helper.createResponse(helper.Success,1,"Deleted",""));
    
 }).catch(err=>{
  res.status(200).send(helper.createResponse(helper.Error,0,err,""));
 })
 
};
